# keycloak setup
This is a basic setup for intgrating the FROST-Server with OpenIDConnect. It should be viewed as an example.
## realm-setup
### 1. add a realm, named: *frostrealm*
### 2. on the realm settings login tab change "Require SSL" to *none*
### 3. create a client for the realm 
   1. Client ID: *frostserver* 
   1. Client Protocol: *openid-connect*

![create client](pics/kc-01.png "create client")

### 4. Root URL: http://{your-DNS}/FROST-Server

![kc-setup](pics/kc-02.png "client settings 1")

### 5. change "Access type" to *confidential*
### 6. enable Authorization

![kc-setup](pics/kc-03.png "client settings 2")

### 7. add realm roles to this realm and some description: 
   1. frost-user, and
   1. frost-admin

![kc-setup](pics/kc-04.png "add realm roles")

### 8. add client roles to the client "frostserver": 
   1. read,
   1. create,
   1. update,
   1. delete,
   1. admin

Notice: These role names correspond to the respective values in the values.yaml of the helm chart (e.g.: http.auth.roleRead: read)

![kc-setup](pics/kc-06.png "add client roles")

### 9.  activate "Composite Roles" for realm roles 
### 10.   add client roles to the newly created realm roles: 
1. **frost-admin** gets *admin* and *delete* client roles
1. **frost-user** gets *create*, *read* and update* client roles

![kc-setup](pics/kc-07.png "add client roles to the realm roles")

## user-setup
### 1. add two users
click on "Add user" and give them a unique name, e.g.
   1. sta-user, 
   1. sta-admin

then hit "Save"

![kc-setup](pics/kc-08.png "add users")

### 2.  click on the newly created user and switch to the details tab, enable the user and set email verified to "On"
![kc-setup](pics/kc-10a.png "user role mappings") 

### 3.switch to the credentials tab: set "Temporary" to *Off*
### 4. switch to "role mappings" tab and assign realm roles: 
   a. frost-admin (realm role) to sta-admin (user)
   b. frost-user to sta-user
![kc-setup](pics/kc-10.png "user role mappings")

## Token configuration
![kc-setup](pics/kc-12.jpg "kc-setup")

Switch to the realm settings and hit "Tokens"-tab. Set the 
1. "Access Token lifespan" to 1 day and the
2. "Offline Session Idle" to  365 days

## Realm secret
### Lastly, switch to the Credentials-tab of the "frostserver"-Client and copy the secret for later use.

![kc-setup](pics/kc-11.jpg "realm secret")
