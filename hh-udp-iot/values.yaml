# If empty, use the chart name as common base name for any FROST-Server Kubernetes component
name:
init:
  image:
    repository: busybox
    tag: 1.31
    pullPolicy: IfNotPresent
  resources: {}
    # limits:
    #   cpu: "10m"
    #   memory: "32Mi"
    # requests:
    #   cpu: "10m"
    #   memory: "32Mi"

clusterDomain: cluster.local
keycloak:
  replicas: 1
  image:
    repository: jboss/keycloak
    tag: 8.0.1
    pullPolicy: IfNotPresent

    ## Optionally specify an array of imagePullSecrets.
    ## Secrets must be manually created in the namespace.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    ##
    pullSecrets: []
    # - myRegistrKeySecretName

  http:
    serviceHost: fqdn.changeme.de
  hostAliases: []
  #  - ip: "1.2.3.4"
  #    hostnames:
  #      - "my.host.com"

  enableServiceLinks: false

  restartPolicy: Always

  serviceAccount:
    # Specifies whether a service account should be created
    create: false
    # The name of the service account to use.
    # If not set and create is true, a name is generated using the fullname template
    name:

  securityContext:
    fsGroup: 1000

  containerSecurityContext:
    runAsUser: 1000
    runAsNonRoot: true

  ## The path keycloak will be served from. To serve keycloak from the root path, use two quotes (e.g. "").
  basepath: auth

  ## Additional init containers, e. g. for providing custom themes
  extraInitContainers: |

  ## Additional sidecar containers, e. g. for a database proxy, such as Google's cloudsql-proxy
  extraContainers: |

  ## lifecycleHooks defines the container lifecycle hooks
  lifecycleHooks: |
    # postStart:
    #   exec:
    #     command: ["/bin/sh", "-c", "ls"]


  ## Additional arguments to start command e.g. -Dkeycloak.import= to load a realm
  extraArgs: ""

  ## Username for the initial Keycloak admin user
  username: keycloak

  ## Password for the initial Keycloak admin user. Applicable only if existingSecret is not set.
  ## If not set, a random 10 characters password will be used
  password: "ChangeMe"

  # Specifies an existing secret to be used for the admin password
  existingSecret: ""

  # The key in the existing secret that stores the password
  existingSecretKey: db.password

  ## Allows the specification of additional environment variables for Keycloak
  extraEnv: |
    - name: PROXY_ADDRESS_FORWARDING
      value: "true"
   # - name: KEYCLOAK_LOGLEVEL
   #   value: DEBUG
   # - name: WILDFLY_LOGLEVEL
   #   value: DEBUG
    # - name: CACHE_OWNERS
    #   value: "2"
    # - name: DB_QUERY_TIMEOUT
    #   value: "60"
    # - name: DB_VALIDATE_ON_MATCH
    #   value: true
    # - name: DB_USE_CAST_FAIL
    #   value: false

  affinity: |
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchLabels:
              {{- include "keycloak.selectorLabels" . | nindent 10 }}
            matchExpressions:
              - key: role
                operator: NotIn
                values:
                  - test
          topologyKey: kubernetes.io/hostname
      preferredDuringSchedulingIgnoredDuringExecution:
        - weight: 100
          podAffinityTerm:
            labelSelector:
              matchLabels:
                {{- include "keycloak.selectorLabels" . | nindent 12 }}
              matchExpressions:
                - key: role
                  operator: NotIn
                  values:
                    - test
            topologyKey: failure-domain.beta.kubernetes.io/zone

  nodeSelector: {}
  priorityClassName: ""
  tolerations: []

  ## Additional pod labels
  ## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
  podLabels: {}

  ## Extra Annotations to be added to pod
  podAnnotations: {}

  livenessProbe:
    initialDelaySeconds: 120
    timeoutSeconds: 5
  readinessProbe:
    initialDelaySeconds: 30
    timeoutSeconds: 1

  resources: {}
    # limits:
    #   cpu: "100m"
    #   memory: "1024Mi"
    # requests:
    #   cpu: "100m"
    #   memory: "1024Mi"

  ## WildFly CLI configurations. They all end up in the file 'keycloak.cli' configured in the configmap which is
  ## executed on server startup.
  cli:
    enabled: true
    nodeIdentifier: |
      {{ .Files.Get "scripts/node-identifier.cli" }}

    logging: |
      {{ .Files.Get "scripts/logging.cli" }}

    ha: |
      {{ .Files.Get "scripts/ha.cli" }}

    datasource: |
      {{ .Files.Get "scripts/datasource.cli" }}

    # Custom CLI script
    custom: |

  ## Custom startup scripts to run before Keycloak starts up
  startupScripts: {}
    # mystartup.sh: |
    #   #!/bin/sh
    #
    #   echo 'Hello from my custom startup script!'

  ## Add additional volumes and mounts, e. g. for custom themes
  extraVolumes: |
  extraVolumeMounts: |

  ## Add additional ports, eg. for custom admin console
  extraPorts: |

  podDisruptionBudget: {}
    # maxUnavailable: 1
    # minAvailable: 1

  service:
    annotations: {}
    # service.beta.kubernetes.io/aws-load-balancer-internal: "0.0.0.0/0"

    labels: {}
    # key: value

    ## ServiceType
    ## ref: https://kubernetes.io/docs/user-guide/services/#publishing-services---service-types
    type: NodePort

    ## Optional static port assignment for service type NodePort.
    # nodePort: 30000

    port: 8080

    # Optional: jGroups port for high availability clustering
    jgroupsPort: 7600

  ## Ingress configuration.
  ## ref: https://kubernetes.io/docs/user-guide/ingress/
  ingress:
    enabled: false
    path: /auth/

    annotations: {}
      # kubernetes.io/ingress.class: nginx
      # kubernetes.io/tls-acme: "true"
      # ingress.kubernetes.io/affinity: cookie

    labels: {}
    # key: value

    ## List of hosts for the ingress
    hosts:
      - fqdn-keycloak.changeme.de

    ## TLS configuration
    tls: 
      - hosts:
          - fqdn-keycloak.changeme.de
        secretName: tls-secret



frost:
  ######################################
  # FROST-Server general configuration #
  ######################################
  enableActuation: false
  enableAuthentication: true
  securityContext:
    fsGroup: 1000

  containerSecurityContext:
    runAsUser: 1000
    runAsNonRoot: true
  ##########################################
  # FROST-Server HTTP module configuration #
  ##########################################
  http:
    image:
      registry: docker.io
      repository: fraunhoferiosb/frost-server-http
      tag: 1.10.1
      pullPolicy: IfNotPresent
    # FROST-Server HTTP deployment settings
    # Horizontal Pod Autoscaler
    hpa:
      enabled: true
      min: 1
      max: 1
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    #-- either you set hpa.enabled: true or you use replicas
    replicas: 1
    ports:
      http:
        nodePort: 
        servicePort: 80
    ingress:
      enabled: false

    # FROST-Server HTTP business settings
    serviceHost: fqdn-frost.changeme.de 
    serviceProtocol: https
    servicePort: 
    urlSubPath: 
    defaultCount: false
    defaultTop: 100
    maxTop: 1000
    maxDataSize: 25000000
    useAbsoluteNavigationLinks: true

    # FROST-Server Database related settings to the FROST-Server HTTP
    db:
      autoUpdate: true
      alwaysOrderbyId: false
      maximumConnection: 10
      maximumIdleConnection: 10
      minimumIdleConnection: 10

    # FROST-Server Messages Bus related settings to the FROST-Server HTTP
    bus:
      sendWorkerPoolSize: 100
      sendQueueSize: 1000
      recvWorkerPoolSize: 10
      recvQueueSize: 100
      maxInFlight: 500

    # FROST-Server Authentication related settings
    auth:
      provider: de.fraunhofer.iosb.ilt.frostserver.auth.keycloak.KeycloakAuthProvider
      allowAnonymousRead: true
      roleRead: read
      roleCreate: create
      roleUpdate: update
      roleDelete: delete
      roleAdmin: admin
      keycloakConfigUrl: https://fqdn-keycloak.changeme.de/auth/realms/frostrealm/clients-registrations/install/frostserver
      keycloackConfigSecret: changeme-secret-from-frostserver-client-in-keycloak
 
  mqtt:
    image:
      registry: docker.io
      repository: fraunhoferiosb/frost-server-mqtt
      tag: 1.10.1
      pullPolicy: IfNotPresent
    # FROST-Server MQTT deployment settings
    enabled: true
    replicas: 1 
    ports:
      mqtt:
        nodePort: 30883
        servicePort: 1883
      websocket:
        nodePort: 30876
        servicePort: 9876
    stickySessionTimeout: 10800

    # FROST-Server MQTT business settings
    qos: 2
    subscribeMessageQueueSize: 1000
    subscribeThreadPoolSize: 20
    createMessageQueueSize: 100
    createThreadPoolSize: 10
    maxInFlight: 50
    waitForEnter: false


    # FROST-Server Database related settings to the FROST-Server MQTT
    db:
      alwaysOrderbyId: false
      maximumConnection: 10
      maximumIdleConnection: 10
      minimumIdleConnection: 10

    # FROST-Server Messages Bus related settings to the FROST-Server MQTT
    bus:
      sendWorkerPoolSize: 10
      sendQueueSize: 100
      recvWorkerPoolSize: 10
      recvQueueSize: 100
      maxInFlight: 50

  ##################################################
  # FROST-Server Messages Bus module configuration #
  ##################################################
  bus:
    image:
        registry: docker.io
        repository: eclipse-mosquitto
        tag: 1.4.12
        pullPolicy: IfNotPresent
    # FROST-Server Messages Bus deployment settings
    ports:
      bus:
        servicePort: 1883

    # Common FROST-Server Messages Bus properties, shared by any other FROST-Server modules
    implementationClass: "de.fraunhofer.iosb.ilt.sta.messagebus.MqttMessageBus"
    topicName: "FROST-Bus"
    qos: 2
##############################################
# FROST-Server Database module configuration, includes keycloak external db connection #
##############################################
db:
  deploy:
    #### config if the db service needs to be deployed in the k8s cluster ####
    enabled: true
  image:
    registry: docker.io
    repository: mdillon/postgis
    tag: 11-alpine
    pullPolicy: IfNotPresent
  # FROST-Server Database deployment settings
  persistence:
    enabled: false
    existingClaim:
    storageClassName:
    accessModes:
      - ReadWriteOnce
    capacity: 10Gi
    # Uncomment bellow if you want to use the builtin "frost-server-db-local" StorageClass name.
    # Only compatible if the ReadWriteOnce access mode is only claimed (check the db.persistence.accessModes value).
    # See project documentation for more information
    #local:
    #  nodeMountPath: /mnt/frost-server-db
    
  # The database vendor. Can be either "postgres", "mysql", "mariadb", or "h2"
  dbVendor: postgres

  
  # Common FROST-Server Database properties, shared by any other FROST-Server modules
  implementationClass: "de.fraunhofer.iosb.ilt.sta.persistence.postgres.longid.PostgresPersistenceManagerLong"
  idGenerationMode: "ServerGeneratedOnly"
  # if ip left blank, it'll take the name of the service
  ip: changeme-postgrsql-ip
  ports:
    postgresql:
      servicePort: 5432

  ## configuration for frost-server ##
  frostConnection:
    database: changeme-sensorthingsdb
    username: changeme-sensorthingsusr 
    password: changeme

  ## configuration for keycloak ##
  keycloakConnection:
    database: changeme-keycloakdb
    username: changeme-keycloakuser
    password: changeme

test:
  enabled: false
  image:
    repository: unguiculus/docker-python3-phantomjs-selenium
    tag: v1
    pullPolicy: IfNotPresent
  securityContext:
    fsGroup: 1000
  containerSecurityContext:
    runAsUser: 1000
    runAsNonRoot: true

##################################################
# Nginx TLS termination module configuration     #
##################################################
nginx-auth:
  enabled: true
  controller:
    config:
      # to use for keycloak redirect issues
      use-forwarded-headers: "true"
    nodeSelector:
      beta.kubernetes.io/os: linux
    autoscaling:
      enabled: true
      minReplicas: 1
      maxReplicas: 1
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    service:
    #  enableHttp: false
    #  enableHttps: false
      type: LoadBalancer
      loadBalancerIP: changeme-external-lb-keycloak-ip
    #metrics:
    #  enabled: true
    #headers: #--- relevant for ip logging in keycloak
  defaultBackend:
    nodeSelector:
      beta.kubernetes.io/os: linux
  #image:
  #  tag: "0.25.0"
  #  pullPolicy: IfNotPresent
    # www-data -> uid 33
  #  runAsUser: 33

  
##################################################
# Nginx TLS termination module configuration     #
##################################################
nginx-frost:
  enabled: true
  tcp: 
    1883: "default/frost-frost-server-mqtt:1883"
  controller:
    config:
      # to use for keycloak redirect issues
      use-forwarded-headers: "true"
    nodeSelector:
      beta.kubernetes.io/os: linux
    autoscaling:
      enabled: true
      minReplicas: 1
      maxReplicas: 1
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    service:
    #  enableHttp: false
    #  enableHttps: false
      type: LoadBalancer
      loadBalancerIP: changeme-external-lb-frost-ip
    #metrics:
    #  enabled: true
    #headers: #--- relevant for ip logging in keycloak
  defaultBackend:
    nodeSelector:
      beta.kubernetes.io/os: linux
  #image:
  #  tag: "0.25.0"
  #  pullPolicy: IfNotPresent
    # www-data -> uid 33
  #  runAsUser: 33

cert:
  # render the extra ca-cert template
  extra: true
  # enable templates ca-cert, ca-clusterissuer, route-ingress
  enabled: true
  # sets acme to staging or prod
  productionEnabled: true
  #eMail: mike@contoso.de
  eMail: changeme@changeme.de
  keycloak:
    keySecretRef: tls-secret
    extraTlsHosts: |
      - fqdn-keycloak.changeme.de 
  frost:
    keySecretRef: tls-secret-frost
    extraTlsHosts: |
      - fqdn-frost.changeme.de
