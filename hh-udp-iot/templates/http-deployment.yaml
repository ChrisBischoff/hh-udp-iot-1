{{- $tier := "http" -}}
{{- $fullName := include "frost-server.fullName" (merge (dict "tier" $tier) .) -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $fullName }}
  labels:
    app.kubernetes.io/managed-by: {{ .Release.Service }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    helm.sh/chart: {{ include "frost-server.chart" . }}
    app: {{ include "frost-server.name" . }}
    component: {{ $tier }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/managed-by: {{ .Release.Service }}
      app.kubernetes.io/instance: {{ .Release.Name }}
      app: {{ include "frost-server.name" . }}
      component: {{ $tier }}
  {{- if not .Values.frost.http.hpa.enabled }}
  replicas: {{ .Values.frost.http.replicas }}
  {{ end }}
  template:
    metadata:
      labels:
        app.kubernetes.io/managed-by: {{ .Release.Service }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        helm.sh/chart: {{ include "frost-server.chart" . }}
        app: {{ include "frost-server.name" . }}
        component: {{ $tier }}
    spec:
      containers:
        - name: {{ $fullName }}
          image: "{{ .Values.frost.http.image.registry }}/{{ .Values.frost.http.image.repository }}:{{ .Values.frost.http.image.tag }}"
          imagePullPolicy: {{ .Values.frost.http.image.pullPolicy | quote }}
          resources:
            requests:
              cpu: 300m
              memory: 900Mi
            limits:
              cpu: 500m
              memory: 1000Mi
          ports:
            - name: tomcat
              containerPort: 8080
          env:
            # Internal properties
            - name: ApiVersion
              value: {{ include "frost-server.http.apiVersion" . | quote }}
            - name: serviceRootUrl
              value: {{ include "frost-server.http.serviceRootUrl" . | quote }}
            - name: enableActuation
              value: "{{ .Values.frost.enableActuation }}"

            # HTTP related properties
            - name: defaultCount
              value: "{{ .Values.frost.http.defaultCount }}"
            - name: defaultTop
              value: "{{ .Values.frost.http.defaultTop }}"
            - name: maxTop
              value: "{{ .Values.frost.http.maxTop }}"
            - name: maxDataSize
              value: "{{ .Values.frost.http.maxDataSize | int64 }}"
            - name: useAbsoluteNavigationLinks
              value: "{{ .Values.frost.http.useAbsoluteNavigationLinks }}"

            {{ if .Values.frost.mqtt.enabled -}}
            # Messages bus related properties
            - name: bus_mqttBroker
              value: {{ printf "tcp://%s:1883" (include "frost-server.fullName" (merge (dict "tier" "bus") .)) | quote }}
            - name: bus_busImplementationClass
              value: "{{ .Values.frost.bus.implementationClass }}"
            - name: bus_topicName
              value: "{{ .Values.frost.bus.topicName }}"
            - name: bus_qosLevel
              value: "{{ .Values.frost.bus.qos }}"
            - name: bus_sendWorkerPoolSize
              value: "{{ .Values.frost.http.bus.sendWorkerPoolSize }}"
            - name: bus_sendQueueSize
              value: "{{ .Values.frost.http.bus.sendQueueSize }}"
            - name: bus_recvWorkerPoolSize
              value: "{{ .Values.frost.http.bus.recvWorkerPoolSize }}"
            - name: bus_recvQueueSize
              value: "{{ .Values.frost.http.bus.recvQueueSize }}"
            - name: bus_maxInFlight
              value: "{{ .Values.frost.http.bus.maxInFlight }}"
            {{- end }}

            # Persistence related properties
            - name: persistence_db_jndi_datasource
              value: ""
            - name: persistence_db_driver
              value: "org.postgresql.Driver"
            - name: persistence_db_url
              value: {{ printf "jdbc:postgresql://%s:5432/%s" (default (include "frost-server.fullName" (merge (dict "tier" "db") .)) .Values.db.ip) .Values.db.frostConnection.database | quote }}
            - name: persistence_persistenceManagerImplementationClass
              value: "{{ .Values.db.implementationClass }}"
            - name: persistence_idGenerationMode
              value: "{{ .Values.db.idGenerationMode }}"
            - name: persistence_autoUpdateDatabase
              value: "{{ .Values.frost.http.db.autoUpdate }}"
            - name: persistence_alwaysOrderbyId
              value: "{{ .Values.frost.http.db.alwaysOrderbyId }}"
            - name: persistence_db_conn_max
              value: "{{ .Values.frost.http.db.maximumConnection }}"
            - name: persistence_db_conn_idle_max
              value: "{{ .Values.frost.http.db.maximumIdleConnection }}"
            - name: persistence_db_conn_idle_min
              value: "{{ .Values.frost.http.db.maximumIdleConnection }}"
            - name: persistence_db_username
              valueFrom:
                secretKeyRef:
                  name: {{ include "frost-server.fullName" . }}-http
                  key: db.username
            - name: persistence_db_password
              valueFrom:
                secretKeyRef:
                  name: {{ include "frost-server.fullName" . }}-http
                  key: db.password

            # authentication related properties
            {{- if .Values.frost.enableAuthentication }}
            - name: auth_provider
              value: "{{ .Values.frost.http.auth.provider }}"
            - name: auth_allowAnonymousRead
              value: "{{ .Values.frost.http.auth.allowAnonymousRead }}"
            - name: auth_role_read
              value: "{{ .Values.frost.http.auth.roleRead }}"
            - name: auth_role_create
              value: "{{ .Values.frost.http.auth.roleCreate }}"
            - name: auth_role_update
              value: "{{ .Values.frost.http.auth.roleUpdate }}"
            - name: auth_role_delete
              value: "{{ .Values.frost.http.auth.roleDelete }}"
            - name: auth_role_admin
              value: "{{ .Values.frost.http.auth.roleAdmin }}"
            - name: auth_keycloakConfigUrl
              value: "{{ .Values.frost.http.auth.keycloakConfigUrl }}"
            - name: auth_keycloakConfigSecret
              value: "{{ .Values.frost.http.auth.keycloackConfigSecret }}"
            {{- end }}
          # Execute some operations before to launch the FROST-Server HTTP's WAR:
          #   1. Remove all existing Tomcat webapps
          #   2. Set the access to the FROST-Server HTTP API at the Tomcat's root
          #   3. Clean environment (remove the unnecessary WAR)
          command:
            - /bin/sh
          args:
            - -c
            - >
              mv /usr/local/tomcat/webapps/FROST-Server.war /tmp
              && rm -rf /usr/local/tomcat/webapps/*
              && unzip -d /usr/local/tomcat/webapps/ROOT /tmp/FROST-Server.war
              && rm /tmp/FROST-Server.war
              && catalina.sh run
