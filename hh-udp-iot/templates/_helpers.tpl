{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "frost-server.name" -}}
{{- default .Chart.Name .Values.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "frost-server.fullName" -}}
{{- $name := default .Chart.Name .Values.name -}}
{{- if .tier -}}
{{- printf "%s-%s-%s" .Release.Name $name .tier | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "frost-server.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Get the HTTP service API version
*/}}
{{- define "frost-server.http.apiVersion" -}}
v1.0
{{- end -}}

{{/*
Get the HTTP service root URL
*/}}
{{- define "frost-server.http.serviceRootUrl" -}}
{{ .Values.frost.http.serviceProtocol }}://{{ .Values.frost.http.serviceHost }}{{ if .Values.frost.http.servicePort }}:{{ .Values.frost.http.servicePort }}{{ else if .Values.frost.http.ingress.enabled }}:{{ .Values.frost.http.ports.http.nodePort }}{{ end }}{{ if .Values.frost.http.urlSubPath }}/{{ .Values.frost.http.urlSubPath }}{{ end }}
{{- end -}}

{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "keycloak.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate to 20 characters because this is used to set the node identifier in WildFly which is limited to
23 characters. This allows for a replica suffix for up to 99 replicas.
*/}}
{{- define "keycloak.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 20 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 20 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 20 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "keycloak.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
{{/*
Create common labels.
*/}}
{{- define "frost-server.commonLabels" -}}
app.kubernetes.io/name: {{ include "frost-server.name" . }}
helm.sh/chart: {{ include "frost-server.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Create selector labels.
*/}}
{{- define "keycloak.selectorLabels" -}}
app.kubernetes.io/name: {{ include "keycloak.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Create name of the service account to use
*/}}
{{- define "keycloak.serviceAccountName" -}}
{{- if .Values.keycloak.serviceAccount.create -}}
    {{ default (include "keycloak.fullname" .) .Values.keycloak.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.keycloak.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create a default fully qualified app name for the postgres requirement.
*/}}

{{- define "keycloak.postgresql.fullname" -}}
{{- $postgresContext := dict "Values" .Values.db "Release" .Release "Chart" (dict "Name" "postgresql") -}}
{{ include "postgresql.fullname" $postgresContext }}
{{- end -}}

{{/*
Create the missing name
*/}}

{{- define "postgresql.fullname" -}}
{{- end -}}}}

{{/*
Create the name for the Keycloak secret.
*/}}
{{- define "keycloak.secret" -}}
{{- if .Values.keycloak.existingSecret -}}
  {{- .Values.keycloak.existingSecret -}}
{{- else -}}
  {{- include "keycloak.fullname" . -}}-http
{{- end -}}
{{- end -}}

{{/*
Create the name for the database secret.
*/}}
{{- define "keycloak.externalDbSecret" -}}
{{- if .Values.keycloak.persistence.existingSecret -}}
  {{- .Values.keycloak.persistence.existingSecret -}}
{{- else -}}
  {{- include "keycloak.fullname" . -}}-db
{{- end -}}
{{- end -}}

{{/*
Create the Keycloak password.
*/}}
{{- define "keycloak.password" -}}
{{- if .Values.keycloak.password -}}
  {{- .Values.keycloak.password | b64enc | quote -}}
{{- else -}}
  {{- randAlphaNum 16 | b64enc | quote -}}
{{- end -}}
{{- end -}}

{{/*
Create the name for the password secret key.
*/}}
{{- define "keycloak.passwordKey" -}}
{{- if .Values.keycloak.existingSecret -}}
  {{- .Values.keycloak.existingSecretKey -}}
{{- else -}}
  db.password
{{- end -}}
{{- end -}}

{{/*
Create the name for the database password secret key.
*/}}
{{- define "frost-server.passwordKey" -}}
{{- if .Values.keycloak.persistence.existingSecret -}}
  {{- .Values.keycloak.persistence.existingSecretKey -}}
{{- else -}}
  db.password
{{- end -}}
{{- end -}}

{{/*
Create environment variables for database configuration.
*/}}
{{- define "keycloak.dbEnvVars" -}}
{{- if .Values.db.deploy.enabled }}
{{- if not (eq "postgres" .Values.db.dbVendor) }}
{{ fail (printf "ERROR: 'Setting keycloak.persistence.deployPostgres' to 'true' requires setting 'keycloak.persistence.dbVendor' to 'postgres' (is: '%s')!" .Values.db.dbVendor) }}
{{- end }}
- name: DB_VENDOR
  value: {{ .Values.db.dbVendor | quote }}
- name: DB_ADDR
  value: {{ (default (include "frost-server.fullName" (merge (dict "tier" "db") .)) .Values.db.ip) | quote }}
- name: DB_PORT
  value: {{ .Values.db.ports.postgresql.servicePort | quote }}
- name: DB_DATABASE
  value: {{ .Values.db.keycloakConnection.database | quote }}
- name: DB_USER
  valueFrom:
    secretKeyRef:
      name: {{ include "frost-server.fullName" . }}-auth
      key: db.username
- name: DB_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ include "frost-server.fullName" . }}-auth
      key: db.password
{{- else }}
- name: DB_VENDOR
  value: {{ .Values.db.dbVendor | quote }}
{{- if not (eq "h2" .Values.db.dbVendor) }}
- name: DB_ADDR
  value: {{ default (include "frost-server.fullName" (merge (dict "tier" "db") .)) .Values.db.ip }}
- name: DB_PORT
  value: {{ .Values.db.ports.postgresql.servicePort }}
- name: DB_DATABASE
  value: {{ .Values.db.keycloakConnection.database | quote }}
- name: DB_USER
  valueFrom:
    secretKeyRef:
      name: {{ include "keycloak.fullName" . }}
      key: "db.username"
- name: DB_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ include "keycloak.fullName" . }}
      key: "db.password"
{{- end }}
{{- end }}
{{- end -}}
