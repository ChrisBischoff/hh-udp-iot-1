# Introduction
This is the Hamburg Urban Data Platform - IoT Helm chart (HH-UDP-IoT). It provides components for the real time data infrastructure of the Urban Data Platform.

It deploys a [**FR**aunhofer **O**pensource **S**ensor**T**hings-Server (FROST-Server)](https://www.iosb.fraunhofer.de/servlet/is/80113/) and Keycloak, an  identity provider for FROST, along with some utility applications to provide an integrated OGC SensorThings API conform, production-ready (TLS-encrypted) environment into your Kubernetes cluster.

The FROST-Server is the first complete, open-source implementation of the OGC SensorThings API standard. Whereas [Keycloak](https://developers.redhat.com/blog/2019/12/11/keycloak-core-concepts-of-open-source-identity-and-access-management/) is an OpenIDConnect-implementation for identity and access management developed by Red Hat. It is used to administer user roles (e.g. read, create, update) and to manage users with different access privileges to the FROST-Server.

For production ready usage this chart installs a [NGINX-Ingress](https://kubernetes.github.io/ingress-nginx/) resource to connect your cloud-provider based Loadbalancers DNS and IP addresses to the FROST-Server and Keycloak instances. Client-Server connections are encrypted by default. The [jetstack cert-manager](https://cert-manager.io/docs/) provides and updates TLS certificates from Let's Encrypt fully automatic.

This Readme provides you with the basic setup and installation steps necessary to install the HH-UDP-IoT Helm chart into your Kubernetes cluster.

# EU-funding notice 

|||
|--|--|
| ![create client](pics/flag_yellow_low.jpg "eu flag")  | This Repository is part of a project that has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 731297.    |

# Prerequisites

* Running [Kubernetes](https://kubernetes.io/) Cluster version 1.14 or 1.15 (later versions are currently not supported by the HH-UDP-IoT helm chart, this will be fixed on later releases, kubernetes versions < 1.14 are not tested)

* Installation of [kubectl](https://kubernetes.io/de/docs/tasks/tools/install-kubectl/)
    * In case of a separate management server with kubectl copy the kubernetes config to the management Server.
    * [copy config howto](https://stackoverflow.com/questions/40447295/how-to-configure-kubectl-with-cluster-information-from-a-conf-file)

* [Helm](https://helm.sh/) Installation on your master or a management server ([Helm installation](https://helm.sh/docs/intro/install/))

* we used HELM 2.x, because with Helm 3.x Versions `helm upgrade` failes (see issue under: https://github.com/helm/helm/issues/6378) 

* Server with [PostgrSQL](https://www.postgresql.org/) minimum 9.5 installed.

* Connectivity between PostgreSQL and the Kubernetes Cluster

* Two external IP-Adresses and two DNS-entries (for the KeyCloak endpoint and for the Frost server)


# Setup steps

* Set up your Kubernetes infrastructure 
    * [MiniKube](https://kubernetes.io/docs/setup/learning-environment/minikube/)
    * [AZURE AKS](https://docs.microsoft.com/en-us/azure/aks/tutorial-kubernetes-deploy-cluster)
    * [AWS EKS](https://aws.amazon.com/en/eks/getting-started/)
    * [Google Cloud Kubernetes Engine](https://cloud.google.com/kubernetes-engine) 

* Set up a PostgreSQL database Server, use version 9.5 or later 
    * see [official Site](https://www.postgresql.org/) or [Ubuntu community site](https://help.ubuntu.com/community/PostgreSQL) for details
    * [install postgis](https://postgis.net/install/)

* [create and prepare the database for keycloak](#Creation-and-preparation-of-KeyCloak-database)

* [add Helm repositories](#add-helm-repositories)

* [deploy official codecentric Keycloak Helm chart for pre configuration purposes](#deploy-the-codecentric-keycloak-helm-chart-for-initial-keycloak-configuration-purposes)

    * [configure Keycloak to use it as auth provider for the FROST-Server](#keycloak-initial-configuration)

* [create and prepare the FROST-Server database](#create-and-configure-the-frost-server-database-on-the-postgres-server)

* [deploy ***HH-UDP-IoT*** Helm Chart](#deployment-of-the-HH-UDP-IoT-helm-chart)

* [verify installation was successful](#environment-check-after-the-installation)


# Creation and preparation of the KeyCloak database 

* connect to the server where your Postgresql database engine is installed

* switch to the postgres user with `sudo su postgres`

* Start the psql cli with `psql`, and run these queries (edit username and password):

```POSTGRESQL
CREATE USER keycloak with encrypted password 'ChangeMe';
CREATE DATABASE keycloak ENCODING=UTF8 owner=keycloak;
```

* Check the postgresql.conf for `listen_addresses = '*'` or modify it to your IP of your postgres server ([look here for details](https://www.postgresql.org/docs/current/runtime-config-connection.html))

* Check the  pg_hba.conf for an  appropriate setting that allows for connections via IPv4 from your kubernetes cluster (example entry in pg_hba.conf:  `host keycloakuser	keycloakDB	192.168.178.99/24	md5`)

* Verify that IPv4 connectivity is possible between your kubernetes cluster and the PostgeSQL server.

# Add Helm Repositories

* connect to the machine where the helm client is installed

* add these Helm Repositories to your helm instance if not already there:

```bash
# stable repo channel
helm repo add stable https://kubernetes-charts.storage.googleapis.com
# contains keycloak
helm repo add codecentric https://codecentric.github.io/helm-charts
# contains cert-manager
helm repo add jetstack https://charts.jetstack.io
# contains the Urban Data Hub IoT chart
helm repo add hh-udp https://api.bitbucket.org/2.0/repositories/geowerkstatt-hamburg/hh-udp-iot/src/master/

# check if these repositories are added
helm repo list
# search for charts in a repo
helm search hh-udp/
#helm repository update
helm repo update
#helm dependecy update
helm helm dependency update
```

# Deploy the [Codecentric Keycloak](https://github.com/codecentric/helm-charts/tree/master/charts/keycloak) Helm Chart for initial keycloak configuration purposes

## Keycloak Values.yaml configuration
Download the values.yaml file and change these values in the to account for your environment and specific needs:

Parameter  | Description | Default
------------- | ------------- | -------------
keycloak.username | Keycloak WebUI admin username  | keycloak
keycloak.password | Keycloak WebUI admin password | ChangeMe
keycloak.persistance.deployPostgres | deploy postgres db container |  false
keycloak.persistance.dbVendor | the database engine used | postgres
keycloak.persistance.dbName | name of the created database | keycloakDB
keycloak.persistance.dbHost | IPv4 of the postgres database server | 
keycloak.persistance.dbPort | tcp port where database engine is listening | 5432
keycloak.persistance.dbUser | name of the database user for the database | keycloakuser

* Change the service type depending on your needs to get an external or internal IP ([ServiceType information](https://kubernetes.io/docs/user-guide/services/#publishing-services---service-types))

Parameter  | Description | Default
------------- | ------------- | -------------
keycloak.service.type| Service type depending on your environment | LoadBalancer

## Keycloak Helm chart Deployment

```bash
# using helm 2.x
helm install -f values.yaml --name keycloak codecentric/keycloak
# using helm 3.x
helm install -f values.yaml keycloak codecentric/keycloak
```

# Keycloak initial configuration
With `keycloak.service.type` set to LoadBalancer your keycloak instance should be reachable by using the IP-Adress of your nodes. Check with:
```
kubectl get svc -w keycloak-http
```

* Connect to https://yourNodeIP:8443/auth/

* Log in with the admin account

* Configure KeyCloak as shown in the KeyCloak setup description [keycloak-setup](keycloak-setup.md)

* additional infos under:
   * https://fraunhoferiosb.github.io/FROST-Server/auth.html
   * https://github.com/FraunhoferIOSB/FROST-Server/issues/117

* after the configuration procedure delete the intermediate helm KeyCloak deployment (but keep the keycloak database on the Postgresql server) with:`helm delete keycloak --purge`

# Create and configure the FROST-Server database on the postgres server
* connect to your database server
* switch to the postgres user with: `sudo su postgres`
* start the psql cli: `psql`
* run (change user and passwords):
```postgresql
-- create user
CREATE USER sensorthings WITH ENCRYPTED PASSWORD 'ChangeMe';
-- create database
CREATE DATABASE sensorthings ENCODING=UTF8 OWNER=sensorthings;
-- check if the database is there
\l;
-- connect to your database
\c sensorthings;
-- activate postgis extension for the FROST-Server database
CREATE EXTENSION postgis;
-- quit psql cli
\q;
```

# Deploy the HH-UDP-IoT Helm Chart
## Preparations
* connect to your machine where kubectl is running
* check that your Kubernetes cluster is up and running and kubectl is connected, i.e. with `kubectl get nodes`

* apply custom resource definitions for the Jetstack cert-manager. These are needed for automatic retrieval of ssl certificates.

    ```bash
    kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml
    ```
    
Configure a .helmignore file to prevent parsing erros during the deployment. https://helm.sh/docs/chart_template_guide/helm_ignore_file/ (add .vscode for example)

## Download and edit the values.yaml

Download the chart's values.yaml with `curl -OL https://api.bitbucket.org/2.0/repositories/geowerkstatt-hamburg/helm-charts/src/master/hh-udp-iot/values.yaml` or go to the repository and store it on your local machine
or 
`curl --user $username https://bitbucket.org/urbandatahub/udh-frost-server-setup/raw/4a05eabc5155016a86fbfa06371b3df4e65ee14b/values.yaml -o values.yaml`

You need to adjust at least these values to account for your environment and your specific needs. Before deploying the HH-UDP-IoT Helm Chart, you need to set up two external IP adresses with DNS entries in your cloud environment. The exact procedure to obtain these ressource depends on your cloud provider.

Parameter  | Description | Default
------------- | ------------- | -------------
keycloak.http.serviceHost  | fqdn of your Keycloak DNS entry  | 
keycloak.username  | Username of the configured admin user  | keycloakuser
keycloak.password | Password for the keycloak admin user | ChangeMe
keycloak.ingress.hosts | fqdn of your Keycloak DNS entry | 
keycloak.ingress.tls.hosts | fqdn of your Keycloak DNS entry |
frost.serviceHost.fqdn | DNS entry for your Frost-Server |
frost.auth.keycloakConfigUrl | client registration endpoint of your FROST-Server (look [here for setup details](keycloak-setup.md)) | http://{FQDN}/realms/{REALMNAME}/clients-registrations/install/{CLIENTNAME}
frost.auth.keycloakConfigSecret | Insert the client secret here that you have  Keycloak config steps | 
frost.db.ip | IP adress of your external postgres database server |  
frost.db.frostConnection.database | name of the created FROST-Server database on the external postgres server | sensorthings
frost.db.frostConnection.username | usernamename of the created user for the FROST-Server database on the external postgres server | sensorthings
frost.db.frostConnection.password | password of the created user for the FROST-Server database on the postgres server | changeMe
frost.db.keycloakConnection.database | name of the created keycloak database on the postgres server | keycloak
frost.db.keycloakConnection.username | username of the created user for the keycloak database on the postgres server | keycloakuser
frost.db.keycloakConnection.password | password of the created user for the keycloak database on the postgres server | ChangeMe
nginx-auth.controller.service.type | LoadBalancer (depending on your environement for external access) | Loadbalancer
nginx-auth.controller.service.loadBalancerIP | external static  IP of your cloud DNS for Keycloak, will be added to the Load balancer Ressource | 
nginx-frost.controller.service.type | (Depending on your environement for external access LoadBalancer) | Loadbalancer
nginx-frost.controller.service.loadBalancerIP | external static  IP of your cloud DNS for the FROST-Server, will be added to the Load balancer Ressource | 
cert.email | email to get certificate information from let's encrypt | 
cert.keycloak.extraTlsHosts | your Keycloak DNS entry | 
cert.frost.extraTlsHosts | your FROST-Server DNS entry | 
nginx-frost.tcp.1883 | service name and location of the mqtt-Service the Ingress Controller searches for, when redirecting requests to the mqtt-Pod, it`s name is composed out of {NAMESPACE}/{HELM-RELEASE}-{CHART.NAME}-mqtt:1883|default/frost-hh-udp-iot-mqtt:1883

### **Note:**
The value for `nginx-frost.tcp.1883` -entry needs to be changed to account for the helm chart release name and the kubernetes namespace used, e.g. for `helm install -f values.yaml --name iot --namespace frost` it needs to be changed to "*iot/frost-hh-udp-iot-mqtt:1883*".


## Install the hh-udp-iot Helm chart

Install with: 

```bash
# using default namespace
helm install -f values.yaml --name frost HH-UDP-IoT
# or deploy to a custom namespace (preferred), i.e.
helm install -f values.yaml --name frostserver --namespace iot hh-udp/HH-UDP-IoT
```
Or, if preferred, you can clone this repo and install the chart from your local system with:
```bash
helm install -f values.yaml --name frostserver --namespace iot ./HH-UDP-IoT
```

# Environment check after the installation

```bash
# check if all services are up and running (getting the external IP's from the cloud loadbalancer may take a few minutes!)
kubectl get svc
# check if the two letsencrypt certificates are "True"
kubectl get certs
# check that the ingress ressources are created
kubectl get ingress
# check that all pods are Running
kubectl get pods
# check the logs of your http / mqtt / default backend pods for any errors
kubectl logs -f {POD-NAME}
```

# Notes
## Aquiring certificates
If you have problems aquiring certificates set the `cert.productionEnabled` to false to use the Let's Encrypt staging environment. Otherwise on multiple retries you probably will hit the LE rate limit (Failed Validation limit of 5 failures per account, per hostname, per hour an production environment). On the LE staging environment this limit is significantly higher.

# Known issues
## helm chart
- Before installing the this helm chart, which installs a keycloak instance too, you need to deploy a stand-alone keycloak instance and preconfigure it. Otherwise the hh-udp-iot Helm chart installation will leave you with some non-functional Pods because the client registration endpoint doesn`t exist yet.
- Currently *db.deploy.enabled* value won't deploy a postgres-container, even if set to **true**; also this value can't currently be set to *false*. If set to *false* the installation with helm may fail.
- The entry for `nginx-frost.tcp.1883` in the values.yaml of this Helm chart won´t adapt to the installation parameters used with `helm install` using a custom namespace and relase name. You have to know these values before installing and enter as shown in [this Note](#note). Otherwise your mqtt Pods can´t be reached from outside of your cluster.
- Messages send to the FROST-Server over the MQTT protocol are currently not tls encrypted.

# More info
- [Urban Data Hub](https://www.hamburg.de/bsw/urban-data-hub/)
- [Urban Data Platform](http://www.urbandataplatform.hamburg/)

# License information

Product  | more Info | Licence
-------- | --------- | --------
FROST-Server  | [Fraunhofer IOSB](https://www.iosb.fraunhofer.de/servlet/is/80113/)  |[GNU](https://github.com/FraunhoferIOSB/FROST-Server/blob/master/LICENSE) |
Keycloak | [keycloak.org](https://www.keycloak.org/) | [Apache License 2.0](https://github.com/keycloak/keycloak/blob/master/LICENSE.txt)
Ingress-NGINX | [kubernetes/ingress-nginx](https://kubernetes.github.io/ingress-nginx/) | [Apache License 2.0](https://github.com/kubernetes/ingress-nginx/blob/master/LICENSE)
Cert-manager | [cert-manager.io](https://cert-manager.io/) | [Apache License 2.0](https://github.com/jetstack/cert-manager/blob/master/LICENSE)